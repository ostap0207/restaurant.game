package com.sysmod.unit;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.sysmod.Barman;
import com.sysmod.Beverage;
import com.sysmod.Chef;
import com.sysmod.Dish;
import com.sysmod.Game;
import com.sysmod.Menu;
import com.sysmod.Order;
import com.sysmod.Owner;
import com.sysmod.Quality;
import com.sysmod.Restaurant;
import com.sysmod.Waiter;

public class Feature4Tests {

	private PrintStream out;

	@Before
	public void setUp() {
		out = Mockito.mock(PrintStream.class);
		System.setOut(out);
	}

	@Test
	public void shouldUpdateBudgetIngredients() {
		Game game = new Game();
		Owner owner = new Owner();
		Restaurant restaurant = new Restaurant();
		restaurant.setOwner(owner);
		owner.setRestaurant(restaurant);
		game.setRestaurant(restaurant);
		restaurant.setBudget(6000);
		initMenu(restaurant);

		restaurant.getWeekOrders().addAll(createOrders(restaurant,4,6,6,4));
		restaurant.getOwner().paySupplier();
		Assert.assertThat(restaurant.getBudget(), equalTo(5920));
	}
	
	public void initMenu(Restaurant restaurant){
		Menu menu = new Menu();

		Dish dish = new Dish();
		dish.setName("hqDish");
		dish.setQuality(Quality.HIGH);
		menu.addDish(dish, 20);

		dish = new Dish();
		dish.setName("lqDish");
		dish.setQuality(Quality.LOW);
		menu.addDish(dish, 20);

		Beverage beverage = new Beverage();
		beverage.setName("hqBeverage");
		beverage.setQuality(Quality.HIGH);
		menu.addBeverage(beverage, 20);

		beverage = new Beverage();
		beverage.setName("lqBeverage");
		beverage.setQuality(Quality.LOW);
		menu.addBeverage(beverage, 20);
		
		restaurant.setMenu(menu);

	}
	
	public List<Order> createOrders(Restaurant restaurant,int hqDish,int lqDish, int hqBeverage, int lqBeverage){
		List<Order> orders = new ArrayList<Order>();
		int total = hqDish + lqDish;
		Menu menu = restaurant.getMenu();
		for (int i = 0; i < total; i++) {
			orders.add(new Order());
		}
		
		for (int i = 0; i < hqDish; i++) {
			orders.get(i).setDish(menu.getDishes().get(0));
		}
		
		for (int i = hqDish; i < total; i++) {
			orders.get(i).setDish(menu.getDishes().get(1));
		}
		
		for (int i = 0; i < hqBeverage; i++) {
			orders.get(i).setBeverage(menu.getBeverages().get(0));
		}
		
		for (int i = hqBeverage; i < total; i++) {
			orders.get(i).setBeverage(menu.getBeverages().get(1));
		}
		
		
		return orders;
	}

	@Test
	public void shouldUpdateBudgetInredients2() {
		Game game = new Game();
		Owner owner = new Owner();
		Restaurant restaurant = new Restaurant();
		restaurant.setOwner(owner);
		owner.setRestaurant(restaurant);
		game.setRestaurant(restaurant);
		restaurant.setBudget(60);
		initMenu(restaurant);

		restaurant.getWeekOrders().addAll(createOrders(restaurant,4,3,3,2));
		restaurant.getOwner().paySupplier();

		Mockito.verify(out).println("Game Over");
	}

	@Test
	public void shouldUpdateBudgetIngredients3() {
		Game game = new Game();
		Owner owner = new Owner();
		Restaurant restaurant = new Restaurant();
		restaurant.setOwner(owner);
		owner.setRestaurant(restaurant);
		game.setRestaurant(restaurant);
		restaurant.setBudget(6000);
		initMenu(restaurant);

		restaurant.getWeekOrders().addAll(createOrders(restaurant,16,2,12,6));
		restaurant.getOwner().paySupplier();

		Assert.assertThat(restaurant.getBudget(), equalTo(5792));
	}

	@Test
	public void shouldUpdateBudgetWeekly() {
		Game game = new Game();
		Owner owner = new Owner();
		Restaurant restaurant = new Restaurant();
		restaurant.setOwner(owner);
		owner.setRestaurant(restaurant);
		game.setRestaurant(restaurant);
		restaurant.setBudget(8000);
		initMenu(restaurant);

		String[] waiters = { "Naved,Ahmed,0,200", "Fabrizio,Maggi,0,200",
				"Amnir,Hadachi,0,200" };
		initEmployees(restaurant,
				new String[]{ "Naved,Ahmed,0,200", "Fabrizio,Maggi,0,200",
				"Amnir,Hadachi,0,200" },
				"Luciano,Garcia,0,300",
				"Abel,Armas,0,300"
				);
		restaurant.getOwner().paySalaries();
		Assert.assertThat(restaurant.getBudget(), equalTo(6800));
	}
	
	@Test
	public void shouldNotUpdateBudgetWeekly() {
		Game game = new Game();
		Owner owner = new Owner();
		Restaurant restaurant = new Restaurant();
		restaurant.setOwner(owner);
		owner.setRestaurant(restaurant);
		game.setRestaurant(restaurant);
		restaurant.setBudget(1000);
		initMenu(restaurant);

		initEmployees(restaurant,
				new String[]{ "Naved,Ahmed,0,200", "Fabrizio,Maggi,0,200",
				"Amnir,Hadachi,0,200" },
				"Luciano,Garcia,0,300",
				"Abel,Armas,0,300"
				);
		restaurant.getOwner().paySalaries();
		Mockito.verify(out).println("Game Over");
	}
	
	@Test
	public void shouldUpdateBudgetWeeklyAndGameOver() {
		Game game = new Game();
		Owner owner = new Owner();
		Restaurant restaurant = new Restaurant();
		restaurant.setOwner(owner);
		owner.setRestaurant(restaurant);
		game.setRestaurant(restaurant);
		restaurant.setBudget(1800);
		initMenu(restaurant);

		initEmployees(restaurant,
				new String[]{ "Naved,Ahmed,0,200", "Fabrizio,Maggi,1,300",
				"Amnir,Hadachi,2,400" },
				"Luciano,Garcia,1,400",
				"Abel,Armas,2,500"
				);
		restaurant.getOwner().paySalaries();
		Assert.assertThat(restaurant.getBudget(), equalTo(0));
		Mockito.verify(out).println("Game Over");
	}
	
	public void initEmployees(Restaurant restaurant, String[] waiters, String chefStr, String barmanStr){
		for (String waiterStr : waiters) {
			String[] data = waiterStr.split(",");
			Waiter waiter = new Waiter(data[0]);
			waiter.setSalary(Integer.parseInt(data[3]));
			int level = Integer.parseInt(data[2]);
			restaurant.addWaiter(waiter);
		}
		
		String[] data = chefStr.split(",");
		Chef chef = new Chef();
		chef.setName(data[0]);
		chef.setSalary(Integer.parseInt(data[3]));
		int level = Integer.parseInt(data[2]);
		restaurant.setChef(chef);
		
		data = barmanStr.split(",");
		Barman barman = new Barman();
		barman.setName(data[0]);
		barman.setSalary(Integer.parseInt(data[3]));
		level = Integer.parseInt(data[2]);
		restaurant.setBarman(barman);
		
	}

}
