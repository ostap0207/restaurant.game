package com.sysmod.unit;

import static org.junit.Assert.*;

import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.sysmod.Beverage;
import com.sysmod.Dish;
import com.sysmod.Game;
import com.sysmod.Menu;
import com.sysmod.Quality;
import com.sysmod.Restaurant;

public class Feature2Tests {

	private PrintStream out;

	@Before
	public void setUp() {
		out = Mockito.mock(PrintStream.class);
		System.setOut(out);
	}

	@Test
	public void shouldCreateMenu() {
		Game game = new Game();
		Restaurant restaurant = new Restaurant();
		game.setRestaurant(restaurant);
		Menu menu = new Menu();
		String[] dishes = { "grilled chicken,400,high,12", "lasagna,800,low,7",
				"gnocchi,700,high,12", "pizza,400,low,7", "snitzel,400,high,12" };
		for (String dishStr : dishes) {
			String[] dishData = dishStr.split(",");
			Dish dish = new Dish();
			dish.setName(dishData[0]);
			dish.setCaloriesCount(Integer.parseInt(dishData[1]));
			dish.setQuality(dishData[2].equals("high") ? Quality.HIGH
					: Quality.LOW);
			menu.addDish(dish, Integer.parseInt(dishData[3]));
		}

		String[] beverages = { "coke,35,high,5", "fanta,35,low,2",
				"wine,50,high,5", "beer,50,low,2", "sprite,35,high,5" };
		for (String beverageStr : beverages) {
			String[] beverageData = beverageStr.split(",");
			Beverage beverage = new Beverage();
			beverage.setName(beverageData[0]);
			beverage.setVolume(Integer.parseInt(beverageData[1]));
			beverage.setQuality(beverageData[2].equals("high") ? Quality.HIGH
					: Quality.LOW);
			menu.addBeverage(beverage, Integer.parseInt(beverageData[3]));
		}
		restaurant.setMenu(menu);

		Mockito.verify(out).println("Restaurant menu is created");
	}

	@Test
	public void shouldNotCreateMenuWithHighQualityDishError() {
		Game game = new Game();
		Restaurant restaurant = new Restaurant();
		game.setRestaurant(restaurant);
		Menu menu = new Menu();
		String[] dishes = { "grilled chicken,400,high,12", "lasagna,800,low,7",
				"gnocchi,700,high,10", "pizza,400,low,7", "snitzel,400,high,12" };
		for (String dishStr : dishes) {
			String[] dishData = dishStr.split(",");
			Dish dish = new Dish();
			dish.setName(dishData[0]);
			dish.setCaloriesCount(Integer.parseInt(dishData[1]));
			dish.setQuality(dishData[2].equals("high") ? Quality.HIGH
					: Quality.LOW);
			menu.addDish(dish, Integer.parseInt(dishData[3]));
		}

		String[] beverages = { "coke,35,high,5", "fanta,35,low,2",
				"wine,50,high,5", "beer,50,low,2", "sprite,35,high,5" };
		for (String beverageStr : beverages) {
			String[] beverageData = beverageStr.split(",");
			Beverage beverage = new Beverage();
			beverage.setName(beverageData[0]);
			beverage.setVolume(Integer.parseInt(beverageData[1]));
			beverage.setQuality(beverageData[2].equals("high") ? Quality.HIGH
					: Quality.LOW);
			menu.addBeverage(beverage, Integer.parseInt(beverageData[3]));
		}
		restaurant.setMenu(menu);

		Mockito.verify(out)
				.println(
						"ERROR!!! All the high quality dishes must have the same price");
	}

	@Test
	public void shouldNotCreateMenuWithLowQualityDishError() {
		Game game = new Game();
		Restaurant restaurant = new Restaurant();
		game.setRestaurant(restaurant);
		Menu menu = new Menu();
		String[] dishes = { "grilled chicken,400,high,12", "lasagna,800,low,7",
				"gnocchi,700,high,12", "pizza,400,low,6", "snitzel,400,high,12" };
		for (String dishStr : dishes) {
			String[] dishData = dishStr.split(",");
			Dish dish = new Dish();
			dish.setName(dishData[0]);
			dish.setCaloriesCount(Integer.parseInt(dishData[1]));
			dish.setQuality(dishData[2].equals("high") ? Quality.HIGH
					: Quality.LOW);
			menu.addDish(dish, Integer.parseInt(dishData[3]));
		}

		String[] beverages = { "coke,35,high,5", "fanta,35,low,2",
				"wine,50,high,5", "beer,50,low,2", "sprite,35,high,5" };
		for (String beverageStr : beverages) {
			String[] beverageData = beverageStr.split(",");
			Beverage beverage = new Beverage();
			beverage.setName(beverageData[0]);
			beverage.setVolume(Integer.parseInt(beverageData[1]));
			beverage.setQuality(beverageData[2].equals("high") ? Quality.HIGH
					: Quality.LOW);
			menu.addBeverage(beverage, Integer.parseInt(beverageData[3]));
		}
		restaurant.setMenu(menu);

		Mockito.verify(out).println(
				"ERROR!!! All the low quality dishes must have the same price");
	}

	@Test
	public void shouldNotCreateMenuWithHighQualityBeverageError() {
		Game game = new Game();
		Restaurant restaurant = new Restaurant();
		game.setRestaurant(restaurant);
		Menu menu = new Menu();
		String[] dishes = { "grilled chicken,400,high,12", "lasagna,800,low,7",
				"gnocchi,700,high,12", "pizza,400,low,7", "snitzel,400,high,12" };
		for (String dishStr : dishes) {
			String[] dishData = dishStr.split(",");
			Dish dish = new Dish();
			dish.setName(dishData[0]);
			dish.setCaloriesCount(Integer.parseInt(dishData[1]));
			dish.setQuality(dishData[2].equals("high") ? Quality.HIGH
					: Quality.LOW);
			menu.addDish(dish, Integer.parseInt(dishData[3]));
		}

		String[] beverages = { "coke,35,high,7", "fanta,35,low,2",
				"wine,50,high,5", "beer,50,low,2", "sprite,35,high,5" };
		for (String beverageStr : beverages) {
			String[] beverageData = beverageStr.split(",");
			Beverage beverage = new Beverage();
			beverage.setName(beverageData[0]);
			beverage.setVolume(Integer.parseInt(beverageData[1]));
			beverage.setQuality(beverageData[2].equals("high") ? Quality.HIGH
					: Quality.LOW);
			if (!menu.addBeverage(beverage, Integer.parseInt(beverageData[3])))
				;
			return;
		}
		restaurant.setMenu(menu);

		Mockito.verify(out)
				.println(
						"ERROR!!! All the high quality beverages must have the same price");
	}

	@Test
	public void shouldNotCreateMenuWithLowQualityBeverageError() {
		Game game = new Game();
		Restaurant restaurant = new Restaurant();
		game.setRestaurant(restaurant);
		Menu menu = new Menu();
		String[] dishes = { "grilled chicken,400,high,12", "lasagna,800,low,7",
				"gnocchi,700,high,12", "pizza,400,low,7", "snitzel,400,high,12" };
		for (String dishStr : dishes) {
			String[] dishData = dishStr.split(",");
			Dish dish = new Dish();
			dish.setName(dishData[0]);
			dish.setCaloriesCount(Integer.parseInt(dishData[1]));
			dish.setQuality(dishData[2].equals("high") ? Quality.HIGH
					: Quality.LOW);
			if (!menu.addDish(dish, Integer.parseInt(dishData[3])))
				;
			return;
		}

		String[] beverages = { "coke,35,high,5", "fanta,35,low,1",
				"wine,50,high,5", "beer,50,low,2", "sprite,35,high,5" };
		for (String beverageStr : beverages) {
			String[] beverageData = beverageStr.split(",");
			Beverage beverage = new Beverage();
			beverage.setName(beverageData[0]);
			beverage.setVolume(Integer.parseInt(beverageData[1]));
			beverage.setQuality(beverageData[2].equals("high") ? Quality.HIGH
					: Quality.LOW);
			if (!menu.addBeverage(beverage, Integer.parseInt(beverageData[3])))
				;
			return;
		}
		restaurant.setMenu(menu);

		Mockito.verify(out)
				.println(
						"ERROR!!! All the low quality beverages must have the same price");
	}
}
