package com.sysmod.unit;

import static org.junit.Assert.*;

import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.sysmod.Game;
import com.sysmod.Player;
import com.sysmod.Restaurant;

public class Feature1Tests {
	private PrintStream out;

	@Before
	public void setUp() {
		out = Mockito.mock(PrintStream.class);
		System.setOut(out);
	}

	@Test
	public void shouldCreateRestaurantForAPlayer() {
		Restaurant restaurant = new Restaurant();
		restaurant.setName("UT Restaurant (UTR)");
		Player player = new Player();
		player.setName("Naved");

		Game game = new Game();
		game.setRestaurant(restaurant);
		game.setPlayer(player);
		game.startGame();
		Mockito.verify(out).println("Welcome to UT Restaurant (UTR) Game!");
		Mockito.verify(out).println("Thank you, Naved.");
		Mockito.verify(out).println("The starting budget equals to 10 000");
	}

}
