package com.sysmod.unit;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.sysmod.Barman;
import com.sysmod.Chef;
import com.sysmod.Employee;
import com.sysmod.Game;
import com.sysmod.Restaurant;
import com.sysmod.Waiter;

public class Feature5Tests {

	private PrintStream out;

	@Before
	public void setUp() {
		out = Mockito.mock(PrintStream.class);
		System.setOut(out);
	}
	
	@Test
	public void shouldTrainChef() {
		Game game = new Game();
		Restaurant restaurant = new Restaurant();
		game.setRestaurant(restaurant);
		restaurant.setBudget(10000);
		initEmployees(restaurant);

		List<Employee> employeesToTrain = new ArrayList<>();
		employeesToTrain.add(restaurant.getChef());

		game.trainEmployees(employeesToTrain);

		Mockito.verify(out).println("The chef experience level increased");
		Assert.assertThat(restaurant.getBudget(), equalTo(8800));
	}

	@Test
	public void shouldTrainBarman() {
		Game game = new Game();
		Restaurant restaurant = new Restaurant();
		game.setRestaurant(restaurant);
		restaurant.setBudget(10000);
		initEmployees(restaurant);

		List<Employee> employeesToTrain = new ArrayList<>();
		employeesToTrain.add(restaurant.getBarman());

		game.trainEmployees(employeesToTrain);

		Mockito.verify(out).println("The barman experience level increased");
		Assert.assertThat(restaurant.getBudget(), equalTo(8800));
	}

	@Test
	public void shouldTrainWaiter() {
		Game game = new Game();
		Restaurant restaurant = new Restaurant();
		game.setRestaurant(restaurant);
		restaurant.setBudget(10000);
		initEmployees(restaurant);

		List<Employee> employeesToTrain = new ArrayList<>();
		for (Waiter waiter : restaurant.getWaiters()) {
			if (waiter.getName().equals("Naved"))
				employeesToTrain.add(waiter);
		}

		game.trainEmployees(employeesToTrain);

		Mockito.verify(out).println("The waiter experience level increased");
		Assert.assertThat(restaurant.getBudget(), equalTo(9200));
	}

	@Test
	public void shouldNotTrainChef() {
		Game game = new Game();
		Restaurant restaurant = new Restaurant();
		game.setRestaurant(restaurant);
		restaurant.setBudget(700);
		initEmployees(restaurant);

		List<Employee> employeesToTrain = new ArrayList<>();
		employeesToTrain.add(restaurant.getChef());

		game.trainEmployees(employeesToTrain);

		Mockito.verify(out).println(
				"The chef experience level failed to increase");
	}

	@Test
	public void shouldTrainTwoWaiters() {
		Game game = new Game();
		Restaurant restaurant = new Restaurant();
		game.setRestaurant(restaurant);
		restaurant.setBudget(10000);
		initEmployees(restaurant);

		List<Employee> employeesToTrain = new ArrayList<>();
		for (Waiter waiter : restaurant.getWaiters()) {
			if (waiter.getName().equals("Fabrizio")
					|| waiter.getName().equals("Amnir"))
				employeesToTrain.add(waiter);
		}

		game.trainEmployees(employeesToTrain);

		Mockito.verify(out).println(
				"The two waiters experience level increased");
		Assert.assertThat(restaurant.getBudget(), equalTo(8400));
	}

	@Test
	public void shouldTrainWaiterAndNotTrainBarman() {
		Game game = new Game();
		Restaurant restaurant = new Restaurant();
		game.setRestaurant(restaurant);
		restaurant.setBudget(880);
		initEmployees(restaurant);

		List<Employee> employeesToTrain = new ArrayList<>();
		for (Waiter waiter : restaurant.getWaiters()) {
			if (waiter.getName().equals("Naved"))
				employeesToTrain.add(waiter);
		}
		employeesToTrain.add(restaurant.getBarman());

		game.trainEmployees(employeesToTrain);

		Mockito.verify(out)
				.println(
						"The waiter experience level increased and the barman experience level failed to increase");
		Assert.assertThat(restaurant.getBudget(), equalTo(80));
	}

	private void initEmployees(Restaurant restaurant) {
		Chef chef = new Chef();
		chef.setName("Luciano");
		restaurant.setChef(chef);

		Barman barman = new Barman();
		barman.setName("Abel");
		restaurant.setBarman(barman);
		restaurant.addWaiter(new Waiter("Naved"));
		restaurant.addWaiter(new Waiter("Fabrizio"));
		restaurant.addWaiter(new Waiter("Amnir"));
	}
}
