package com.sysmod.unit;

import static org.junit.Assert.*;

import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.sysmod.Game;
import com.sysmod.Restaurant;
import com.sysmod.Waiter;

public class Feature6Tests {

	private PrintStream out;

	@Before
	public void setUp() {
		out = Mockito.mock(PrintStream.class);
		System.setOut(out);
	}
	
	@Test
	public void shouldAssignTablesSuccessfully() {
		Game game = new Game();
		Restaurant restaurant = new Restaurant();
		game.setRestaurant(restaurant);
		
		Waiter waiterNaved = new Waiter("Naved");
		Waiter waiterFabrizio = new Waiter("Fabrizio");
		Waiter waiterAmnir = new Waiter("Amnir");
		restaurant.addWaiter(waiterNaved);
		restaurant.addWaiter(waiterFabrizio);
		restaurant.addWaiter(waiterAmnir);
		
		game.assignTable(waiterNaved, restaurant.getTables().get(2 -1));
		game.assignTable(waiterNaved, restaurant.getTables().get(9 -1));
		game.assignTable(waiterAmnir, restaurant.getTables().get(4 -1));
		game.assignTable(waiterFabrizio, restaurant.getTables().get(6 -1));
		game.assignTable(waiterFabrizio, restaurant.getTables().get(1 -1));
		Mockito.verify(out, Mockito.atLeastOnce()).println(
				"Successful Assignment");
		
	}
	
	@Test
	public void shouldNotAssignTablesSuccessfully() {
		Game game = new Game();
		Restaurant restaurant = new Restaurant();
		game.setRestaurant(restaurant);
		
		Waiter waiterNaved = new Waiter("Naved");
		Waiter waiterFabrizio = new Waiter("Fabrizio");
		Waiter waiterAmnir = new Waiter("Amnir");
		restaurant.addWaiter(waiterNaved);
		restaurant.addWaiter(waiterFabrizio);
		restaurant.addWaiter(waiterAmnir);
		
		game.assignTable(waiterNaved, restaurant.getTables().get(1 -1));
		game.assignTable(waiterNaved, restaurant.getTables().get(2 -1));
		game.assignTable(waiterNaved, restaurant.getTables().get(3 -1));
		game.assignTable(waiterFabrizio, restaurant.getTables().get(5 -1));
		game.assignTable(waiterNaved, restaurant.getTables().get(4 -1));
		
		Mockito.verify(out).println(
				"ERROR!!! Naved has more than three tables assigned");
		
	}

}
