package com.sysmod.acceptance;

/**
 * Created by ostap_000 on 13/11/2014.
 */
public class EmployeeData {

    String name;
    String lastname;
    String job;
    String experience;

    public EmployeeData(String name, String job, String experience) {
        this.name = name;
        this.job = job;
        this.experience = experience;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }
}
