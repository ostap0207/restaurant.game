package com.sysmod.acceptance;

import com.sysmod.*;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import org.junit.Assert;
import org.mockito.Mockito;

import gherkin.formatter.model.DataTableRow;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;

public class GameSteps {

	Restaurant restaurant;
	Player player;
	Game game;

	PrintStream out;
	private List<DishData> dishes;
	private List<BeverageData> beverages;
	private List<EmployeeData> employees;
	List<Order> orders;

	@Before
	public void setup() {
		orders = new ArrayList<Order>();
		out = Mockito.mock(PrintStream.class);
		System.setOut(out);
	}

	@Given("^The \"([^\"]*)\" is created with the name \"([^\"]*)\"$")
	public void The_is_created_with_the_name(String type, String name)
			throws Throwable {
		switch (type) {
		case "restaurant":
			restaurant = new Restaurant();
			restaurant.setName(name);
			break;
		case "player":
			player = new Player();
			player.setName(name);
			break;
		}
	}

	@Given("^The restaurant budget is initialised to (\\d+)$")
	public void The_restaurant_budget_is_initialised_to(int budget)
			throws Throwable {
		restaurant.setBudget(budget);
	}

	@When("^I start playing restaurant game$")
	public void I_start_playing_restaurant_game() throws Throwable {
		game = new Game();
		game.setPlayer(player);
		game.setRestaurant(restaurant);
		game.startGame();
	}

	@Then("^I should see \"([^\"]*)\"$")
	public void I_should_see(String msg) throws Throwable {
		Mockito.verify(out).println(msg);
	}

	@Given("^The game has started$")
	public void the_game_has_started() throws Throwable {
		game = new Game();
		game.setPlayer(new Player());
		
	}

	@Given("^The restaurant is created$")
	public void the_restaurant_is_created() throws Throwable {
		restaurant = new Restaurant();
		game.setRestaurant(restaurant);
		
		Owner owner = new Owner();
		restaurant.setOwner(owner);
		owner.setRestaurant(restaurant);

		Menu menu = new Menu();

		Dish dish = new Dish();
		dish.setName("hqDish");
		dish.setQuality(Quality.HIGH);
		menu.addDish(dish, 20);

		dish = new Dish();
		dish.setName("lqDish");
		dish.setQuality(Quality.LOW);
		menu.addDish(dish, 20);

		Beverage beverage = new Beverage();
		beverage.setName("hqBeverage");
		beverage.setQuality(Quality.HIGH);
		menu.addBeverage(beverage, 20);

		beverage = new Beverage();
		beverage.setName("lqBeverage");
		beverage.setQuality(Quality.LOW);
		menu.addBeverage(beverage, 20);
		
		restaurant.setMenu(menu);

	}

	@Given("^the following set of \"(.*?)\"$")
	public void the_following_set_of(String type, DataTable data)
			throws Throwable {

		if (type.equals("Dishes")) {
			dishes = data.asList(DishData.class);
		} else {
			beverages = data.asList(BeverageData.class);
		}

	}

	@When("^I create a Menu$")
	public void i_create_a_Menu() throws Throwable {
		Menu menu = new Menu();
		for (DishData dishData : dishes) {
			Dish dish = new Dish();
			dish.setName(dishData.getDishName());
			dish.setCaloriesCount(dishData.getCalories());
			dish.setQuality(dishData.getQuality().equals("high") ? Quality.HIGH
					: Quality.LOW);
			if (!menu.addDish(dish, dishData.getCost()))
				return;
		}

		for (BeverageData beverageData : beverages) {
			Beverage beverage = new Beverage();
			beverage.setName(beverageData.getBeverageName());
			beverage.setVolume(beverageData.getVolume());
			beverage.setQuality(beverageData.getQuality().equals("high") ? Quality.HIGH
					: Quality.LOW);
			if (!menu.addBeverage(beverage, beverageData.getCost()))
				return;
		}
		restaurant = new Restaurant();
		restaurant.setMenu(menu);
	}

	@Then("^I should see the \"(.*?)\"$")
	public void i_should_see_the(String msg) throws Throwable {
		Mockito.verify(out, Mockito.atLeastOnce()).println(msg);
	}

	@Given("^The budget is (\\d+)$")
	public void The_budget_is(int budget) throws Throwable {
		restaurant.setBudget(budget);
	}

	@Given("^cost of ingredients for \"([^\"]*)\" is (\\d+)$")
	public void cost_of_ingredients_for_is(String foodType, int price)
			throws Throwable {
		PriceList priceList = restaurant.getMenu().getPriceList();
		switch (foodType) {
		case "high quality dishes":
			priceList.setHighDishIngPrice(price);
			break;
		case "low quality dishes":
			priceList.setLowDishIngPrice(price);
			break;
		case "high quality beverages":
			priceList.setHighBeverageIngPrice(price);
			break;
		case "low quality beverages":
			priceList.setLowBeverageIngPrice(price);
			break;
		}
	}

	@Given("^(\\d+) clients choose \"([^\"]*)\"$")
	public void clients_choose(int amount, String foodType) throws Throwable {

			switch (foodType) {
			case "high quality dishes":
				for (int i = 0; i < amount; i++) {
				Order order = new Order();
				order.setDish(restaurant.getMenu().getDishes().get(0));
					orders.add(order);
				}
				break;
			case "low quality dishes":
				for (int i = 0; i < amount; i++) {
					Order order = new Order();
					order.setDish(restaurant.getMenu().getDishes().get(1));
					orders.add(order);
				}
				break;
			case "high quality beverages":
				for (int i = 0; i < amount; i++) {
					Order order = orders.get(i);
					order.setBeverage(restaurant.getMenu().getBeverages().get(0));
				}
				break;
			case "low quality beverages":
				int begin = 0;
				while (orders.get(begin).getBeverage() != null)
					begin++;
				for (int i = begin; i < begin + amount; i++) {
					Order order = orders.get(i);
					order.setBeverage(restaurant.getMenu().getBeverages().get(1));
				}
				break;
			}
		
	}

	
	@When("^The budget is updated based on the \"([^\"]*)\"$")
	public void The_budget_is_updated_based_on_the(String type)
			throws Throwable {
		if (type.equals("expenses")) {
			restaurant.getWeekOrders().addAll(orders);
			restaurant.getOwner().paySupplier();
		} else {
			restaurant.getOwner().paySalaries();
		}
	}

	@Then("^The budget should be (\\d+)$")
	public void The_budget_should_be(int budget) throws Throwable {
		Assert.assertThat(restaurant.getBudget(), equalTo(budget));
	}

	@Then("^Game Over$")
	public void Game_Over() throws Throwable {
		Mockito.verify(out).println("Game Over");
	}

	@Given("^The restaurant has the following employee\\(s\\) as \"([^\"]*)\"$")
	public void The_restaurant_has_the_following_employee_s_as(String job,
			DataTable data) throws Throwable {
		List<EmployeeData2> employees = data.asList(EmployeeData2.class);
		if (job.equals("chef")) {
			Chef chef = new Chef();
			initEmployee2(chef, employees.get(0));
			chef.setTaxCode(Integer.parseInt(employees.get(0).getTaxCode()));
			restaurant.setChef(chef);
		} else if (job.equals("barman")) {
			Barman barman = new Barman();
			initEmployee2(barman, employees.get(0));
			restaurant.setBarman(barman);
		} else if (job.equals("waiters")) {
			for (int i = 0; i < 3; i++) {
				EmployeeData2 employeeData = employees.get(i);
				Waiter waiter = new Waiter(employeeData.getName());
				initEmployee2(waiter, employeeData);
				restaurant.addWaiter(waiter);
			}
		}
	}

	@Given("^The list of employees$")
	public void The_list_of_employees(DataTable data) throws Throwable {
		game = new Game();
		restaurant = new Restaurant();
		game.setRestaurant(restaurant);
		employees = data.asList(EmployeeData.class);
		for (EmployeeData employeeData : employees) {
			if (employeeData.getJob().equals("barman")) {
				Barman barman = new Barman();
				initEmployee(barman, employeeData);
				restaurant.setBarman(barman);
			} else if (employeeData.getJob().equals("chef")) {
				Chef chef = new Chef();
				initEmployee(chef, employeeData);
				restaurant.setChef(chef);
			} else {
				Waiter waiter = new Waiter(employeeData.getName());
				restaurant.addWaiter(waiter);
			}
		}
	}

	public void initEmployee2(Employee employee, EmployeeData2 employeeData) {
		employee.setName(employeeData.getName());
		employee.setSurname(employeeData.getSurname());
		employee.setSalary(employeeData.getSalary());
	}

	public void initEmployee(Employee employee, EmployeeData employeeData) {
		employee.setName(employeeData.getName());
	}

	@When("^I want to increase the experience of (\\d+) employee\\(s\\) \"([^\"]*)\" with the job \"([^\"]*)\"$")
	public void I_want_to_increase_the_experience_of_employee_s_with_the_job(
			int amount, String namesStr, String jobsStr) throws Throwable {
		String[] jobs = jobsStr.split(",");
		String[] names = namesStr.split(",");
		List<Employee> employees = new ArrayList<>();
		for (int i = 0; i < amount; i++) {
			String job = jobs[i];

			if (job.equals("barman")) {
				employees.add(restaurant.getBarman());
			} else if (job.equals("chef")) {
				employees.add(restaurant.getChef());
			} else {
				for (Waiter waiter : restaurant.getWaiters()) {
					if (waiter.getName().equals(names[i])) {
						employees.add(waiter);
						continue;
					}
				}
			}

		}

		game.trainEmployees(employees);
	}

	@Then("^The budget should update to (\\d+)$")
	public void The_budget_should_update_to(int budget) throws Throwable {
		Assert.assertThat(restaurant.getBudget(), equalTo(budget));
	}

	@Given("^The restaurant has following tables$")
	public void The_restaurant_has_following_tables(List<String> data) throws Throwable {

		List<Table> tables = new ArrayList<Table>();

		for (String row : data){
			if (!row.equals("tableNumber")){
				Table table = new Table(Integer.parseInt(row));
				tables.add(table);
			}
		}
		restaurant.setTables(tables);
	}

	@Given("^the following clients are randomly assigned to the tables$")
	public void the_following_clients_are_randomly_assigned_to_the_tables(DataTable data) throws Throwable {
		List<TableClient> assignments = data.asList(TableClient.class);
	}

	@Given("^The restaurant has (\\d+) waiters \"([^\"]*)\" all with experience level (\\d+) and salary (\\d+)$")
	public void The_restaurant_has_waiters_all_with_experience_level_and_salary(int amount, String namesStr, int level, int salary) throws Throwable {
		String[] names = namesStr.split(",");
		for (int i = 0;i < amount; i++){
			Waiter waiter = new Waiter(names[i].trim().split(" ")[0]);
			waiter.setSalary(200);
			restaurant.addWaiter(waiter);
		}
	}

	@When("^I assigned the following waiters to the table$")
	public void I_assigned_the_following_waiters_to_the_table(DataTable data) throws Throwable {
		List<TableClient> assignments = data.asList(TableClient.class);
		for (TableClient assignment : assignments){
			
			game.assignTable(getWaiter(assignment.assigned_waiter), restaurant.getTables().get(assignment.tableNumber - 1));
		}	
	}
	
	public Waiter getWaiter(String name){
		for (Waiter waiter :restaurant.getWaiters()){
			if (waiter.getName().equals(name))
				return waiter;
		}
		return null;
	}
	



}
