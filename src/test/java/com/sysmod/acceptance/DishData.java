package com.sysmod.acceptance;

public class DishData {
	
	private String dishName;
	private Integer calories;
	private String quality;
	private Integer cost;
	
	public String getDishName() {
		return dishName;
	}
	public void setDishName(String name) {
		this.dishName = name;
	}
	public Integer getCalories() {
		return calories;
	}
	public void setCalories(Integer calories) {
		this.calories = calories;
	}
	public String getQuality() {
		return quality;
	}
	public void setQuality(String quality) {
		this.quality = quality;
	}
	public Integer getCost() {
		return cost;
	}
	public void setCost(Integer price) {
		this.cost = price;
	}
	public DishData(String name, Integer calories, String quality, Integer price) {
		super();
		this.dishName = name;
		this.calories = calories;
		this.quality = quality;
		this.cost = price;
	}
	
	
}
