package com.sysmod.acceptance;

public class BeverageData {

	private String beverageName;
	private Integer volume;
	private String quality;
	private Integer cost;
	
	public String getBeverageName() {
		return beverageName;
	}
	public void setBeverageName(String name) {
		this.beverageName = name;
	}
	public Integer getVolume() {
		return volume;
	}
	public void setVolume(Integer volume) {
		this.volume = volume;
	}
	public String getQuality() {
		return quality;
	}
	public void setQuality(String quality) {
		this.quality = quality;
	}
	public Integer getCost() {
		return cost;
	}
	public void setCost(Integer price) {
		this.cost = price;
	}
	public BeverageData(String name, Integer volume, String quality, Integer price) {
		super();
		this.beverageName = name;
		this.volume = volume;
		this.quality = quality;
		this.cost = price;
	}
	
	
	
}
