/**
 * @(#) Game.java
 */

package com.sysmod;

import java.text.DecimalFormat;
import java.util.*;

public class Game {

    private Player player;

    private static List<Owner> scoreList = new ArrayList<Owner>();

    private Restaurant restaurant;
    private boolean playing = false;

    public Owner createOwner() {
        Owner owner = new Owner();
        owner.setName(player.getName());
        return owner;
    }

    public Restaurant createRestaurant() {
        Restaurant restaurant = new Restaurant();
        restaurant.setName(printQuestionAndGetAnswer("What's the name of restaurant?"));
        restaurant.setAddress(printQuestionAndGetAnswer("What's the address of restaurant?"));
        restaurant.setCity(printQuestionAndGetAnswer("What's the city of restaurant?"));
        return restaurant;
    }

    public void startGame() {
        Owner owner = createOwner();
        if (restaurant == null)
            restaurant = createRestaurant();
        restaurant.setOwner(owner);
        restaurant.initWaiters();
        owner.setRestaurant(restaurant);

        System.out.println(String.format("Welcome to %s Game!", restaurant.getName()));
        System.out.println(String.format("Thank you, %s.", player.getName()));
        DecimalFormat myFormatter = new DecimalFormat("###,###");
        System.out.println(String.format("The starting budget equals to %s", myFormatter.format(restaurant.getBudget()).replaceAll(","," ")));
    }

    public void stopGame() {
        System.exit(0);
    }

    public void playGame() {
        playing = true;
        manageMenu();

        for (int i = 0; i < 30; i++) {
            if (!playing)
                return;
            startDay(i + 1);
            endDay(i + 1);
        }
    }

    public void startDay(int dayNumber) {
        clearConsole();
        System.out.println("#########################################################################");
        System.out.println("Day " + dayNumber);
        System.out.println("#########################################################################");
        showRestaurantState(restaurant);
        System.out.println("#########################################################################");
        showTrainingMenu(restaurant);

        int command = printMenu("How would you like to assign tables to waiters?", new String[]{"Autoassignment", "Manually"});
        if (command == 0) {
            assignTableAutomatically();
        } else {
            assignTableManually();
        }

        pickOrders();
    }

    public void pickOrders(){
        int tableAmount = restaurant.getOccupiedTablesAmount();

        List<Client> clients = new ArrayList<Client>(restaurant.getClients());
        Collections.shuffle(clients);

        for (int i = 0; i < tableAmount; i ++) {
            Client client1 = clients.get(2*i);
            Client client2 = clients.get(2*i+1);
            restaurant.makeOrder(client1, client2);
            System.out.println("Order " + i);
        }
    }

    public void endDay(int day) {
        restaurant.accountDayProfit();
        restaurant.adjustDaySatisfaction();
        restaurant.transferDayOrders();
        restaurant.clearTables();

        if (day % 7 == 0) {
            boolean salariesPaid = restaurant.getOwner().paySalaries();
            boolean supplierPaid = restaurant.getOwner().paySupplier();
            if (!salariesPaid || !supplierPaid)
                endGame(false);
            restaurant.transferWeekOrders();
        }

        if (day % 30 == 0) {
            endGame(true);
        }

    }

    public void showRestaurantState(Restaurant restaurant) {
        System.out.println("----------------------------------------------------------------------");
        System.out.println("Budget :" + restaurant.getBudget());
        System.out.println("Reputation :" + restaurant.getReputation());
        System.out.println("----------------------------------------------------------------------");
    }

    public void showTrainingMenu(Restaurant restaurant) {
        boolean training = true;
        while (training) {
            int command = printMenu("Would you like to train someone", new String[]{"Yes", "No"});
            if (command == 0) {
                int person = printMenu("Who would you like to train", new String[]{"Chef", "Barman", "Waiters"});
                Employee toTrain = null;
                switch (person) {
                    case 0:
                        toTrain = restaurant.getChef();
                        break;
                    case 1:
                        toTrain = restaurant.getBarman();
                        break;
                    case 2:
                        int waiter = printMenu("Which waiter would you like to train", new String[]{"1", "2", "3"});
                        toTrain = restaurant.getWaiters().get(waiter);
                        break;
                }
                training = !trainEmployee(restaurant, toTrain);
            } else {
                training = false;
            }
        }
    }

    public void manageMenu() {
        Menu menu = new Menu();
        System.out.println("Decide the menu for your restaurant.");
        System.out.println("Add 5 dishes.");
        while (menu.getAmountOfDishes() < 5) {
            Dish dish = new Dish();
            dish.setName(printQuestionAndGetAnswer("Enter dish name"));
            dish.setCaloriesCount(Integer.valueOf(printQuestionAndGetIntAnswer("Enter calories")));
            dish.setQuality(printMenu("Choose quality", new String[]{"HIGH", "LOW"}) == 0 ? Quality.HIGH : Quality.LOW);
            int price = printQuestionAndGetIntAnswer("Enter price");
            if (!menu.addDish(dish,price))
                System.out.println("Dish already exists. Try again");
            else
                System.out.println("Dish successfully added");
        }

        while (menu.getAmountOfBeverages() < 5) {
            Beverage beverage = new Beverage();
            beverage.setName(printQuestionAndGetAnswer("Enter beverage name"));
            beverage.setVolume(Integer.valueOf(printQuestionAndGetIntAnswer("Enter volume")));
            beverage.setQuality(printMenu("Choose quality", new String[]{"HIGH", "LOW"}) == 0 ? Quality.HIGH : Quality.LOW);
            int price = printQuestionAndGetIntAnswer("Enter price");
            if (menu.addBeverage(beverage,price))
                System.out.println("Beverage successfully added");
            else
                System.out.println("Beverage successfully added");
        }
        
        restaurant.setMenu(menu);
    }

    public List<Table> popTables(Stack<Table> tables, int number) {
        List<Table> result = new ArrayList<Table>();
        for (int i = 0; i < number; i++) {
            result.add(tables.pop());
        }
        return result;
    }

    public void assignTable(Waiter waiter, Table table) {
    	if (waiter.getTables().size() <= 2){
	        waiter.getTables().add(table);
	        table.setWaiter(waiter);
	        System.out.println("Successful Assignment");
    	} else {
    		System.out.println(String.format("ERROR!!! %s has more than three tables assigned",waiter.getName()));
    	}
        
    }

    public void showNotification(String message) {
        System.out.println("--------------------------" + message + "----------------------------");
    }

    public List<Table> getavailableTables(Restaurant restaurant) {
        List<Table> tables = new ArrayList<Table>();
        for (Table table : restaurant.getTables()) {
            if (table.getWaiter() == null) {
                tables.add(table);
            }
        }
        return tables;
    }

    public void assignTableAutomatically() {
        int tableAmount = restaurant.getOccupiedTablesAmount();
        List<Waiter> waiters = new ArrayList<Waiter>(restaurant.getWaiters());
        Collections.sort(waiters, new Comparator<Waiter>() {
            @Override
            public int compare(Waiter o1, Waiter o2) {
                return o1.getLevelOfExperience().level - o2.getLevelOfExperience().level;
            }
        });

        Stack<Table> tables = new Stack<Table>();
        tables.addAll(restaurant.getTables());

        for (Waiter waiter : waiters) {
            waiter.setTables(null);
            if (tableAmount > 0) {
                waiter.setTables(new ArrayList<Table>());
                List<Table> chosenTables = popTables(tables,Math.min(3,tableAmount));
                for (Table table : chosenTables) {
                    assignTable(waiter, table);
                }
                tableAmount -= waiter.getTables().size();
            }
        }
    }

    public void assignTableManually() {
        int tableAmount = restaurant.getOccupiedTablesAmount();
        Stack<Table> tables = new Stack<Table>();
        tables.addAll(restaurant.getTables());
        for (Waiter waiter : restaurant.getWaiters()) {
            System.out.println("Available tables : " + tableAmount);
            int maxTables = Math.min(3, tableAmount);
            String[] tablesOptions = new String[maxTables + 1];
                for (int i = 0; i <= maxTables; i++) {
                    tablesOptions[i] = i + "";
                }

                int result = printMenu("How many tables assign to waiter " + waiter.getName() + " with experience level :" + waiter.getLevelOfExperience().toString(), tablesOptions);
                List<Table> chosenTables = popTables(tables,result);
                for (Table table : chosenTables) {
                    assignTable(waiter, table);
                }
                tableAmount -= result;
        }
    }

    public void showError(String error) {
        System.out.println("!!!!!!!!!!!!!!!" + error + "!!!!!!!!!!!!!!!");
    }


    public void endGame(boolean success) {
        playing = false;
        boolean finalResult = false;
        if (success) {
           finalResult = restaurant.decreaseBudget(4000);
        }

        if (finalResult) {
            showNotification("You win!");
            scoreList.add(restaurant.getOwner());
        }else{
            showNotification("You lose!");
        }
        showStatistics();
    }

    public boolean trainEmployee(Restaurant restaurant, Employee employee) {
        List<Employee> employees = new ArrayList<>();
        employees.add(employee);
        return trainEmployees(employees);
    }

    public void showStatistics() {
        System.out.println("----------------------------------------------------------------------");
        showNotification("Statistic");
        System.out.println("----------------------------------------------------------------------");
        List<Client> clients = restaurant.getClients();
        for (Client client : clients) {
            System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
            System.out.println(client.getName());
            System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
            Statistic statistic = restaurant.getClientStatistic(client);
            System.out.println("Consumed dishes");
            for (String dish : statistic.getDishMap().keySet()) {
                System.out.println(dish + " : " + statistic.getDishMap().get(dish));
            }
            System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@Os");
            System.out.println("Consumed beverages");
            for (String beverage : statistic.getBeverageMap().keySet()) {
                System.out.println(beverage + " : " + statistic.getBeverageMap().get(beverage));
            }

            System.out.println("Total amount spent : " + statistic.getTotalAmountSpent());
            System.out.println("Average calories in dish : " + statistic.getAvgCalories());
            System.out.println("Average volume in beverage : " + statistic.getAvgVolume());
            System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");

        }
        System.out.println("----------------------------------------------------------------------");
    }


    public static void main(String[] args) {
        Player player = new Player();
        player.setName(printQuestionAndGetAnswer("What's your name?"));

        boolean exit = false;
        while(!exit) {
            int command = printMenu("What would you like to do?", new String[]{"Start game", "See score board", "Exit"});
            if (command == 0) {
                Restaurant restaurant = new Restaurant();
                restaurant.setName(printQuestionAndGetAnswer("Enter restaurant name:"));
                Game game = new Game();
                game.setPlayer(player);
                game.setRestaurant(restaurant);
                game.startGame();
            } else if (command == 1) {
                if (scoreList.size() == 0)
                    System.out.println("Score board - empty");
                else{
                    Collections.sort(scoreList, new Comparator<Owner>() {
                        @Override
                        public int compare(Owner o1, Owner o2) {
                            return o1.getRestaurant().getBudget() - o2.getRestaurant().getBudget();
                        }
                    });
                    for (int i = 0; i < scoreList.size(); i++) {
                        Owner owner = scoreList.get(i);
                        System.out.println((i + 1) + ". " + owner.getName() + " ------------------------ " + owner.getRestaurant().getBudget());
                    }
                }
            }else{
                System.out.println("Bye!");
                exit = true;
            }
        }
    }


    private static int printMenu(String title, ArrayList<String> commands) {
        String[] arrayCommands = new String[commands.size()];
        arrayCommands = commands.toArray(arrayCommands);
        return printMenu(title, arrayCommands);
    }

    private static int printMenu(String title, String[] commands) {
        clearConsole();
        System.out.println("----------------------------------------------------------------------");
        System.out.println(title);
        System.out.println("----------------------------------------------------------------------");
        boolean chosen = false;
        int result = 0;
        Scanner scanner = new Scanner(System.in);
        while (!chosen) {
            boolean inputError = false;
            for (int i = 0; i < commands.length; i++) {
                System.out.println((i + 1) + ". " + commands[i]);
            }
            System.out.println("----------------------------------------------------------------------");
            try {
                result = Integer.parseInt(scanner.next()) - 1;
            }catch (NumberFormatException ex){
                inputError = true;
            }
            if (result > commands.length - 1 || inputError)
                System.out.println("Wrong command, try again.");
            else
                chosen = true;
        }
        return result;
    }

    private static String printQuestionAndGetAnswer(String question) {
        clearConsole();
        System.out.println("----------------------------------------------------------------------");
        System.out.println(question);
        System.out.println("----------------------------------------------------------------------");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    private static int printQuestionAndGetIntAnswer(String question) {
        clearConsole();
        Scanner scanner = new Scanner(System.in);
        boolean answered = false;
        int result = 0;
        while(!answered) {
            System.out.println("----------------------------------------------------------------------");
            System.out.println(question);
            System.out.println("----------------------------------------------------------------------");

            try {
                result = Integer.parseInt(scanner.next()) - 1;
            } catch (NumberFormatException ex) {
                System.out.println("Not a number! Try again.");
                continue;
            }
            answered = true;
        }
        return result;
    }

    public final static void clearConsole() {
        try {
            final String os = System.getProperty("os.name");
                Runtime.getRuntime().exec("cls");
                Runtime.getRuntime().exec("clear");
        } catch (final Exception e) {
        }
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

    public boolean trainEmployees(List<Employee> employeesToTrain) {
        employeesToTrain.sort(Comparator.comparingInt(e -> e.getTrainingCost()));
        boolean result = true;
        List<Employee> success = new ArrayList<>();
        List<Employee> fail = new ArrayList<>();
        StringBuilder status = new StringBuilder("The");
        for (Employee employee : employeesToTrain) {

            if (employee.getLevelOfExperience() == Level.HIGH) {
                fail.add(employee);
                result = false;
                continue;
            }

            int cost = employee.getTrainingCost();
            int budget = restaurant.getBudget();

            if (budget < cost) {
                fail.add(employee);
                result = false;
                continue;
            }

            boolean isSuccessful = restaurant.decreaseBudget(cost);
            if (!isSuccessful) {
                fail.add(employee);
                result = false;
                continue;
            }

            employee.increaseExperienceLevel();
            success.add(employee);
        }

        if (success.size() > 0)
            status.append(" ").append(getEmployeesTitle(success)).append(" experience level increased");

        if (fail.size() > 0) {
            if (success.size() > 0)
                status.append(" and the");
            status.append(" ").append(getEmployeesTitle(fail)).append(" experience level failed to increase");
        }

        System.out.println(status.toString());
        return result;
    }

    private String getEmployeesTitle(List<Employee> employees){
        HashMap<String,Integer> map = new HashMap<>();
        StringBuilder title = new StringBuilder();
        for (Employee employee : employees) {
            String type = getEmployeeType(employee);
            map.put(type,map.getOrDefault(type,0) + 1);
        }
        boolean first = true;
        for (String s : map.keySet()) {
            if (!first)
                title.append(",");

            title.append(toPlural(map.get(s), s));
            first = false;
        }
        return title.toString();
    }

    private String toPlural(int number, String word){
        if (number == 1)
            return word;
        if (number == 2)
            return "two " + word + "s";
        if (number == 3)
            return "tree " + word + "s";
        return "";
    }

    private String getEmployeeType(Employee employee){
        if (employee instanceof Chef) return "chef";
        if (employee instanceof Barman) return "barman";
        if (employee instanceof Waiter) return "waiter";
        return "";
    }
}
