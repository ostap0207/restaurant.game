/**
 * @(#) Employee.java
 */

package com.sysmod;

public abstract class Employee extends Person {

    public void setSalary(Integer salary) {
		Salary = salary;
	}

	private Integer Salary;

    private Level LevelOfExperience = Level.LOW;

    private Player player;

    public Employee(Integer salary, String name) {
        super(name);
        Salary = salary;
    }

    public Level getLevelOfExperience() {
        return LevelOfExperience;
    }

    public abstract Integer getTrainingCost();

    public void increaseExperienceLevel() {
        if (LevelOfExperience == Level.LOW)
            LevelOfExperience = Level.MEDIUM;
        else if (LevelOfExperience == Level.MEDIUM)
            LevelOfExperience = Level.HIGH;
    }

    public Integer getSalary() {
        int adjustedSalary = Salary;
        if (getLevelOfExperience() == Level.MEDIUM)
            adjustedSalary += 100;
        if (getLevelOfExperience() == Level.HIGH)
            adjustedSalary +=  200;
        return adjustedSalary;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

    public abstract int getSatisfactionLevel();

}
