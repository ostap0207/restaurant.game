/**
 * @(#) Chef.java
 */

package com.sysmod;

public class Chef extends Employee {
    private Integer TaxCode;

    public Chef() {
        super(300, "Chef 1");
    }

    public void setTaxCode(Integer taxCode) {
        TaxCode = taxCode;
    }

    public Integer getTaxCode() {
        return TaxCode;
    }

    @Override
    public Integer getTrainingCost() {
        return 1200;
    }

    @Override
    public int getSatisfactionLevel() {
        if (getLevelOfExperience() == Level.HIGH) return 80;
        if (getLevelOfExperience() == Level.MEDIUM) return 60;
        return 40;
    }


}
