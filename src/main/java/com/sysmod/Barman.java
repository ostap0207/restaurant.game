/**
 * @(#) Barman.java
 */

package com.sysmod;

public class Barman extends Employee {

    public Barman() {
        super(300, "Barman 1");
    }

    @Override
    public Integer getTrainingCost() {
        return 1200;
    }

    @Override
    public int getSatisfactionLevel() {
        if (getLevelOfExperience() == Level.HIGH) return 80;
        if (getLevelOfExperience() == Level.MEDIUM) return 60;
        return 40;
    }


}
