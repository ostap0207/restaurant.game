/**
 * @(#) Waiter.java
 */

package com.sysmod;

import java.util.ArrayList;
import java.util.List;

public class Waiter extends Employee {


    public Waiter(String name) {
        super(200, name);
        orders = new ArrayList<Order>();
        tables = new ArrayList<Table>();
    }

    @Override
    public Integer getTrainingCost() {
        return 800;
    }

    private List<Order> orders;

    private List<Table> tables;


    public List<Order> getOrders() {
        return orders;
    }


    public List<Table> getTables() {
        return tables;
    }


    public void setTables(List<Table> tables) {
        this.tables = tables;
    }


    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }


    public void clearTables(){
        if (tables == null) return;
        for (Table table : tables) {
            table.clear();
            table.setWaiter(null);
        }
    }

    public int getSatisfactionLevel(){
        if (getLevelOfExperience() == Level.HIGH) return 90;
        if (getLevelOfExperience() == Level.MEDIUM) return 80;
        return 60;
    }

}
