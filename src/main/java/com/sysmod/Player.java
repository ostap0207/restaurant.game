/**
 * @(#) Player.java
 */

package com.sysmod;

public class Player {
    private Game game;

    private String name;

    private Employee trainedEmployees;

    private Restaurant restaurant;

    public void setGame(Game game) {
        this.game = game;
    }

    public Game getGame() {
        return game;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


}
