package com.sysmod;

/**
 * Created by ostap_000 on 11/1/2014.
 */
public enum Level {
    HIGH(3), MEDIUM(2), LOW(1);

    public int level;

    Level(int level){
        this.level = level;
    }
}
