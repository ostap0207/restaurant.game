/**
 * @(#) Client.java
 */

package com.sysmod;

import java.util.ArrayList;
import java.util.List;

public class Client extends Person {


    private Integer TaxCode;

    private Restaurant restaurant;

    private List<Order> orders;

    public Client(String name) {
        super(name);
        orders = new ArrayList<Order>();
    }

    public void setTaxCode(Integer taxCode) {
        TaxCode = taxCode;
    }

    public Integer getTaxCode() {
        return TaxCode;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }


    public List<Order> getOrders() {
        return orders;
    }


}
