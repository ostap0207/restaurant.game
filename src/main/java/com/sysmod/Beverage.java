/**
 * @(#) Beverage.java
 */

package com.sysmod;

public class Beverage extends Food {

    private Integer Volume;

    public void setVolume(Integer volume) {
        Volume = volume;
    }

    public Integer getVolume() {
        return Volume;
    }

}
