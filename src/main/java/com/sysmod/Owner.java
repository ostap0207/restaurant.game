package com.sysmod;

import java.util.List;

/**
 * Created by ostap_000 on 11/1/2014.
 */
public class Owner {

    private Restaurant restaurant;

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    private String name;

    private String lastname;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public boolean paySalaries() {
        int salariesSum = 0;
        salariesSum += restaurant.getChef().getSalary();
        salariesSum += restaurant.getBarman().getSalary();
        for (Waiter waiter : restaurant.getWaiters()) {
            salariesSum += waiter.getSalary();
        }

        return restaurant.decreaseBudget(salariesSum);

    }

    public boolean paySupplier() {
        int supplierSum = 0;
        List<Order> orders = restaurant.getWeekOrders();
        Menu menu = restaurant.getMenu();
        PriceList priceList = menu.getPriceList();
        for (Order order : orders) {
            supplierSum += priceList.getDishIngPrice(order.getDish());
            supplierSum += priceList.getBeverageIngPrice(order.getBeverage());
        }

        return restaurant.decreaseBudget(supplierSum);
    }

}
