/**
 * @(#) Table.java
 */

package com.sysmod;

import java.util.ArrayList;
import java.util.List;

public class Table {

    public Table(Integer number) {
        Number = number;
        clients = new ArrayList<Client>(2);
    }

    private Integer Number;

    private List<Client> clients;

    private Waiter waiter;

    public void setNumber(Integer number) {
        Number = number;
    }

    public Integer getNumber() {
        return Number;
    }

    public void setWaiter(Waiter waiter) {
        this.waiter = waiter;
    }

    public Waiter getWaiter() {
        return waiter;
    }

    public void setClients(List<Client> clients) {
        this.clients = clients;
    }

    public List<Client> getClients() {
        return clients;
    }

    public void clear(){
        clients.clear();
    }


}
