/**
 * @(#) Restaurant.java
 */

package com.sysmod;

import java.util.*;

public class Restaurant {

    private Random random = new Random();

    private Owner owner;

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    private String Name;

    private String Address;

    private String City;

    private Integer Budget = 10000;

    private float reputation = 15;

    private List<Client> clients;

    private Menu menu;

    private List<Waiter> waiters;

    private List<Table> tables;

    public List<Table> getTables() {
        return tables;
    }

    public void setTables(List<Table> tables) {
        this.tables = tables;
    }

    private Barman barman;

    private Chef chef;

    private Player player;

    private List<Order> orders;

    private List<Order> weekOrders;

    private List<Order> dayOrders;

    public Restaurant() {
        barman = new Barman();
        chef = new Chef();

        waiters = new ArrayList<Waiter>();

        tables = new ArrayList<Table>();
        for (int i = 0; i < 9; i++) {
            tables.add(new Table(i));
        }

        menu = new Menu();

        clients = new ArrayList<Client>();
        for (int i = 0; i < 18; i++) {
            clients.add(new Client("Client " + i));
        }

        dayOrders = new ArrayList<Order>();
        weekOrders = new ArrayList<Order>();
        orders = new ArrayList<Order>();
    }

    public void initWaiters(){
        for (int i = 0; i < 3; i++) {
            addWaiter(new Waiter("Waiter " + i));
        }
    }

    public Integer getBudget() {
        return Budget;
    }

    public boolean decreaseBudget(Integer amount) {
        if (Budget < amount){
        	System.out.println("Game Over");
            return false;
        }
        Budget -= amount;
        
        if (Budget == 0)
        	System.out.println("Game Over");
        
        return true;
    }

    public void makeOrder(Client client1, Client client2) {
        Table table = getFreeTable();
        if (table == null) return;

        table.getClients().add(client1);
        table.getClients().add(client2);

        Order order1 = new Order();
        order1.setWaiter(table.getWaiter());
        order1.setClient(client1);
        client1.getOrders().add(order1);
        chooseFood(order1);

        Order order2 = new Order();
        order2.setWaiter(table.getWaiter());
        order2.setClient(client2);
        client2.getOrders().add(order2);
        chooseFood(order2);

        registerOrder(order1);
        registerOrder(order2);
    }

    public Table getFreeTable() {
        for (Table table : tables) {
            if (table.getClients().size() == 0 && table.getWaiter() != null)
                return table;
        }
        return null;
    }

    public void chooseFood(Order order) {
        Dish dish = menu.getDishes().get(random.nextInt(5));
        Beverage beverage = menu.getBeverages().get(random.nextInt(5));
        order.setDish(dish);
        order.setBeverage(beverage);
    }

    public void registerOrder(Order order) {
        dayOrders.add(order);
    }

    public void transferWeekOrders(){
        orders.addAll(weekOrders);
        weekOrders.clear();
    }

    public void transferDayOrders(){
        weekOrders.addAll(dayOrders);
        dayOrders.clear();
    }

    public void accountDayProfit(){
        int dayProfit = 0;
        PriceList priceList = menu.getPriceList();
        for (Order dayOrder : dayOrders) {
            int dishProfit = getFoodProfit(dayOrder.getDish());
            int beverageProfit = getFoodProfit(dayOrder.getBeverage());
            dayProfit += dishProfit;
            dayProfit += beverageProfit;
        }

        Budget += dayProfit;
    }

    public void adjustDaySatisfaction(){
        double totalSatisfaction = 0;
        for (Order dayOrder : dayOrders) {
            double satisfaction = 0;
            Waiter waiter = dayOrder.getWaiter();
            satisfaction += randSatisfaction(waiter.getSatisfactionLevel());

            int chefSatisfactionLevel = chef.getSatisfactionLevel() + (dayOrder.getDish().getQuality() == Quality.HIGH ? 20 : 0);
            satisfaction += randSatisfaction(chefSatisfactionLevel);

            int barmanSatisfactionLevel = barman.getSatisfactionLevel() + (dayOrder.getBeverage().getQuality() == Quality.HIGH ? 20 : 0);
            satisfaction += randSatisfaction(barmanSatisfactionLevel);

            int decreasePercentage = 0;
            int dishProfit = getFoodProfit(dayOrder.getDish());
            decreasePercentage += (dishProfit / 3) * 10;

            int beverageBenefit = getFoodProfit(dayOrder.getBeverage());
            decreasePercentage += (beverageBenefit / 3) * 10;
            decreasePercentage = Math.max(0,decreasePercentage);
            decreasePercentage = Math.min(1,decreasePercentage);

            satisfaction = satisfaction * (100 - decreasePercentage) / 100;
            totalSatisfaction += satisfaction;
        }
        adjustReputation(totalSatisfaction);
    }

    private int getFoodProfit(Food food){
        PriceList priceList = menu.getPriceList();
        if (food instanceof Dish) {
            Dish dish = (Dish) food;
            return priceList.getDishPrice(dish) - priceList.getDishIngPrice(dish);
        }else{
            Beverage beverage = (Beverage) food;
            return priceList.getBeveragePrice(beverage) - priceList.getBeverageIngPrice(beverage);
        }
    }

    public int randSatisfaction(int percentage){
        if (random.nextInt(100) < percentage){
            return 1;
        }else{
            return -1;
        }
    }

    public void setSatisfaction() {

    }

    public void calculateAmountCalories() {

    }

    public void calculateConsumedFood() {

    }

    public void calculateMoneySpent() {

    }

    public void setName(String name) {
        Name = name;
    }

    public String getName() {
        return Name;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getAddress() {
        return Address;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getCity() {
        return City;
    }

    public void setBudget(Integer budget) {
        Budget = budget;
    }

    public void adjustReputation(double amount) {
        reputation += amount;
    }

    public float getReputation() {
        return reputation;
    }

    public Level getReputationLevel() {
        if (reputation < 15) return Level.LOW;
        if (reputation < 30) return Level.MEDIUM;
        return Level.HIGH;
    }

    public void setClients(List<Client> clients) {
        this.clients = clients;
    }

    public List<Client> getClients() {
        return clients;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
        System.out.println("Restaurant menu is created");
    }

    public Menu getMenu() {
        return menu;
    }

    public void setWaiters(List<Waiter> waiters) {
        this.waiters = waiters;
    }

    public List<Waiter> getWaiters() {
        return waiters;
    }

    public void setBarman(Barman barman) {
        this.barman = barman;
    }

    public Barman getBarman() {
        return barman;
    }

    public void setChef(Chef chef) {
        this.chef = chef;
    }

    public Chef getChef() {
        return chef;
    }

    public void clearTables(){
        for (Waiter waiter : waiters) {
            waiter.clearTables();
        }
    }

    public List<Order> getWeekOrders() {
        return weekOrders;
    }

    public Statistic getClientStatistic(Client client){
        Statistic statistic = new Statistic();

        PriceList priceList = menu.getPriceList();
        List<Order> orders = client.getOrders();

        int totalAmount = 0;
        double totalCalories = 0;
        double totalVolume = 0;

        for (Order order : orders) {
            Dish dish = order.getDish();
            Beverage beverage = order.getBeverage();

            String dishName = dish.getName();
            String beverageName = beverage.getName();
            if (!statistic.getDishMap().containsKey(dishName))
                statistic.getDishMap().put(dishName,0);
            statistic.getDishMap().put(dishName,statistic.getDishMap().get(dishName) + 1);
            totalAmount += priceList.getDishPrice(dish);
            totalCalories += dish.getCaloriesCount();

            if (!statistic.getBeverageMap().containsKey(beverageName))
                statistic.getBeverageMap().put(beverageName,0);
            statistic.getBeverageMap().put(beverageName,statistic.getBeverageMap().get(beverageName) + 1);
            totalAmount += priceList.getBeveragePrice(beverage);
            totalVolume += beverage.getVolume();
        }

        statistic.setTotalAmountSpent(totalAmount);
        statistic.setAvgCalories(totalCalories/orders.size());
        statistic.setAvgVolume(totalVolume/orders.size());
        return statistic;
    }

    public int getOccupiedTablesAmount(){
        int tableAmount = 9;
        if (getReputationLevel() == Level.LOW)
            tableAmount = 2;
        else if (getReputationLevel() == Level.MEDIUM)
            tableAmount = 5;
        return tableAmount;
    }

    public void addWaiter(Waiter waiter) {
        waiters.add(waiter);
    }
}
