/**
 * @(#) Dish.java
 */

package com.sysmod;

public class Dish extends Food {
    private Integer CaloriesCount;

    public void setCaloriesCount(Integer caloriesCount) {
        CaloriesCount = caloriesCount;
    }

    public Integer getCaloriesCount() {
        return CaloriesCount;
    }

}
