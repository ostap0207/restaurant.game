/**
 * @(#) Statistic.java
 */

package com.sysmod;

import java.util.HashMap;
import java.util.Map;

public class Statistic {

    private Map<String, Integer> dishMap;
    private Map<String, Integer> beverageMap;
    private int totalAmountSpent;
    private double avgCalories;
    private double avgVolume;

    public Statistic() {
        dishMap = new HashMap<String, Integer>();
        beverageMap = new HashMap<String, Integer>();
    }

    public Map<String, Integer> getDishMap() {
        return dishMap;
    }

    public void setDishMap(Map<String, Integer> dishMap) {
        this.dishMap = dishMap;
    }

    public Map<String, Integer> getBeverageMap() {
        return beverageMap;
    }

    public void setBeverageMap(Map<String, Integer> beverageMap) {
        this.beverageMap = beverageMap;
    }

    public int getTotalAmountSpent() {
        return totalAmountSpent;
    }

    public void setTotalAmountSpent(int totalAmountSpent) {
        this.totalAmountSpent = totalAmountSpent;
    }

    public double getAvgCalories() {
        return avgCalories;
    }

    public void setAvgCalories(double avgCalories) {
        this.avgCalories = avgCalories;
    }

    public double getAvgVolume() {
        return avgVolume;
    }

    public void setAvgVolume(double avgVolume) {
        this.avgVolume = avgVolume;
    }
}
