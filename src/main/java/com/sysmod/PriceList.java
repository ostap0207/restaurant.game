package com.sysmod;

/**
 * Created by ostap_000 on 11/1/2014.
 */
public class PriceList {

    public static int HIGH_DISH_ING = 10;
    public static int LOW_DISH_ING = 3;
    public static int HIGH_BEVERAGE_ING = 3;
    public static int LOW_BEVERAGE_ING = 1;

    private int highDishPrice;
    private int lowDishPrice;
    private int highBeveragePrice;
    private int lowBeveragePrice;

    public int getHighDishPrice() {
        return highDishPrice;
    }

    public void setHighDishPrice(int highDishPrice) {
        this.highDishPrice = highDishPrice;
    }

    public int getLowDishPrice() {
        return lowDishPrice;
    }

    public void setLowDishPrice(int lowDishPrice) {
        this.lowDishPrice = lowDishPrice;
    }

    public int getHighBeveragePrice() {
        return highBeveragePrice;
    }

    public void setHighBeveragePrice(int highBeveragePrice) {
        this.highBeveragePrice = highBeveragePrice;
    }

    public int getLowBeveragePrice() {
        return lowBeveragePrice;
    }

    public void setLowBeveragePrice(int lowBeveragePrice) {
        this.lowBeveragePrice = lowBeveragePrice;
    }

    public int getDishIngPrice(Dish dish){
        if (dish.getQuality() == Quality.HIGH)
            return HIGH_DISH_ING;
        return LOW_DISH_ING;
    }

    public int getBeverageIngPrice(Beverage beverage){
        if (beverage.getQuality() == Quality.HIGH)
            return HIGH_BEVERAGE_ING;
        return LOW_BEVERAGE_ING;
    }

    public int getDishPrice(Dish dish){
        if (dish.getQuality() == Quality.HIGH)
            return getHighDishPrice();
        return getLowDishPrice();
    }

    public int getBeveragePrice(Beverage beverage){
        if (beverage.getQuality() == Quality.HIGH)
            return getHighBeveragePrice();
        return getLowBeveragePrice();
    }

	public void setHighDishIngPrice(int price) {
		HIGH_DISH_ING = price;
	}
	
	public void setLowDishIngPrice(int price) {
		LOW_DISH_ING = price;
	}
	
	public void setHighBeverageIngPrice(int price) {
		HIGH_BEVERAGE_ING = price;
	}
	
	public void setLowBeverageIngPrice(int price) {
		LOW_BEVERAGE_ING = price;
	}

}
