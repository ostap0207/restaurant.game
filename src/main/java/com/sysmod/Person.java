/**
 * @(#) Person.java
 */

package com.sysmod;

public class Person {
    public Person(String name) {
        Name = name;
    }

    protected String Name;

    protected String Surname;

    public String getName() {
        return Name;
    }

    public String getSurname() {
        return Surname;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public void setSurname(String surname) {
        this.Surname = surname;
    }


}
