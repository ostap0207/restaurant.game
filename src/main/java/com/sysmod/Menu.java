/**
 * @(#) Menu.java
 */

package com.sysmod;

import java.util.ArrayList;
import java.util.List;

public class Menu {
    private List<Dish> dishes;

    public Menu() {
        dishes = new ArrayList<Dish>();
        beverages = new ArrayList<Beverage>();
        priceList = new PriceList();
    }

    private List<Beverage> beverages;

    private PriceList priceList;

    public PriceList getPriceList() {
        return priceList;
    }

    public void setPriceList(PriceList priceList) {
        this.priceList = priceList;
    }

    public Integer getAmountOfDishes() {
        return dishes.size();
    }

    public boolean addDish(Dish dish,Integer price) {
        if (isDishExists(dish))
            return false;
        
        if (dish.getQuality() == Quality.HIGH){
        	int highDishPrice = priceList.getHighDishPrice();
        	if (highDishPrice != 0 && highDishPrice != price){
        		printError("high", "dishes");
        		return false;
        	}
        	priceList.setHighDishPrice(price);	
        }else if (dish.getQuality() == Quality.LOW){
        	int lowDishPrice = priceList.getLowDishPrice();
        	if (lowDishPrice != 0 && lowDishPrice != price){
        		printError("low", "dishes");
        		return false;
        	}
        	priceList.setLowDishPrice(price);	
        }
        
        acceptDish(dish);
        return true;
    }

    public Boolean isDishExists(Dish dish) {
        for (Dish dish1 : dishes) {
            if (dish1.getName().equals(dish.getName()))
                return true;
        }
        return false;
    }

    public List<Beverage> getBeverages() {
        return beverages;
    }

    public List<Dish> getDishes() {
        return dishes;
    }

    private void acceptBeverage(Beverage beverage) {
        beverages.add(beverage);
    }

    public Boolean isBeverageExists(Beverage beverage) {
        for (Beverage beverage1 : beverages) {
            if (beverage1.getName().equals(beverage.getName()))
                return true;
        }
        return false;
    }

    public int getAmountOfBeverages() {
        return beverages.size();
    }

    public boolean addBeverage(Beverage beverage, Integer price) {
        if (isBeverageExists(beverage))
            return false;
        
        if (beverage.getQuality() == Quality.HIGH){
        	int highPrice = priceList.getHighBeveragePrice();
        	if (highPrice != 0 && highPrice != price){
        		printError("high", "beverages");
        		return false;
        	}
        	priceList.setHighBeveragePrice(price);	
        }else if (beverage.getQuality() == Quality.LOW){
        	int lowPrice = priceList.getLowBeveragePrice();
        	if (lowPrice != 0 && lowPrice != price){
        		printError("low", "beverages");
        		return false;
        	}
        	priceList.setLowBeveragePrice(price);	
        }
        
        acceptBeverage(beverage);
        return true;
    }

    private void acceptDish(Dish dish) {
        dishes.add(dish);
    }
    
    private void printError(String quality, String food){
    	System.out.println("ERROR!!! All the " + quality + " quality " + food+ " must have the same price");
    }

}
